Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config
  root 'welcome#index'
  resources :sales
  resources :suppliers
  resources :expenses
  resources :receipts
  resources :shops
  resources :products
  resources :buyers
  resources :providers
  resources :sellers
  resources :reports
  resources :stocks
  devise_for :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
