json.extract! stock, :id, :shop_id, :product_id, :count, :price, :created_at, :updated_at
json.url stock_url(stock, format: :json)
