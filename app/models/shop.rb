class Shop < ApplicationRecord
  has_many :expenses
  has_many :receipts
end
