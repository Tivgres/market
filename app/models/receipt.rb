class Receipt < ApplicationRecord
  belongs_to :shop
  belongs_to :seller
  belongs_to :buyer
end
