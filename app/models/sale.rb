# frozen_string_literal: true

class Sale < ApplicationRecord
  belongs_to :shop
  belongs_to :seller
  has_one :receipt
  has_one :supplier
  has_one :buyer
  has_one :product

  validates :product_id, presence: true

  before_save :add_receipt
  after_destroy :delete_receipt

  def add_receipt
    shop = Shop.find(shop_id)
    if !shop.style.eql?('booths') && !shop.style.eql?('shopfront')
      receipt = Receipt.create!(seller_id: seller_id, shop_id: shop_id, product_id: product_id, buyer_id: buyer_id, price: price, sale_id: id)
      self.receipt_id =  receipt.id
    end
  end

  def delete_receipt
    Receipt.find(receipt_id).destroy! unless receipt_id.nil?
  end
end
