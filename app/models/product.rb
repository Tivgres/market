class Product < ApplicationRecord
  validates :title, uniqueness: true, presence: true
end
