class Seller < User
  has_many :receipts
  has_many :sales
end
