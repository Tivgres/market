class StocksController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :stock_params_update, only: %w[update]
  before_action :stock_params_create, only: %w[create]
  before_action :find_stocks, only: %w[index]

  def index
    redirect_to root_path unless current_user.is_a? Seller
  end

  def new
    @stock = Stock.new
    @products = Product.all
  end

  def create
    stock = Stock.new stock_params_create
    supplier = Supplier.where(product_id: stock_params_create[:product_id]).order('price').first
    stock.price = supplier.price
    stock.shop_id = current_user.shop_id
    if stock.save!
      flash[:success] = 'Success'
      redirect_to stocks_path
    else
      flash[:dangr] = 'Some error'
      redirect_to new_stock_path
    end
  end

  def edit
    @stock = Stock.find(params[:id])
  end

  def update
    current_stock = Stock.where(shop_id: current_user.shop_id, product_id: stock_params_update[:product_id]).first
    if stock_params_update[:count].to_i <= current_stock.count
      if Stock.where(shop_id: stock_params_update[:shop_id], product_id: stock_params_update[:product_id]).exists?
        another_stock = Stock.where(shop_id: stock_params_update[:shop_id], product_id: stock_params_update[:product_id]).first
        another_stock.count = another_stock.count + stock_params_update[:count].to_i
        another_stock.price = (another_stock.price + current_stock.price) / 2
        if another_stock.save!
          change_current_stock current_stock
          flash[:success] = 'Success'
          redirect_to stocks_path
        else
          flash[:danger] = "Unknown error"
          redirect_to edit_stock_path(current_stock.id)
        end
      else
        Stock.create!(shop_id: stock_params_update[:shop_id], product_id: stock_params_update[:product_id], count: stock_params_update[:count], price: current_stock.price)
        change_current_stock current_stock
        flash[:success] = 'Success'
        redirect_to stocks_path
      end
    else
      flash[:danger] = "To many count, maximum: #{current_stock.count}"
      redirect_to edit_stock_path(current_stock.id)
    end
  end

  private

  def change_current_stock(stock)
    if stock.count == stock_params_update[:count].to_i
      stock.destroy!
    else
      stock.count = stock.count - stock_params_update[:count].to_i
      stock.save!
    end
  end

  def find_stocks
    @stocks = Stock.where(shop_id: current_user.shop_id).page(params[:page]).per(25)
  end

  def stock_params_create
    params.require(:stock).permit(:product_id, :count)
  end

  def stock_params_update
    params.require(:stock).permit(:shop_id, :product_id, :count)
  end

  def stock_params
    params.require(:stock).permit(:id)
  end
end

