# frozen_string_literal: true

class WelcomeController < ApplicationController
  before_action :check_user

  def index; end

  private

  def check_user
    if current_user
      case current_user
      when Seller
        redirect_to sales_path
      when Buyer
        redirect_to shops_path
      when Provider
        suppliers_path(current_user.id)
      end
    end
  end
end
