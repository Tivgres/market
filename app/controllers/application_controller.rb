class ApplicationController < ActionController::Base

  def after_sign_in_path_for(resource)
    if resource.is_a? User
      case resource
      when AdminUser
        admin_dashboard_path(resource)
      when Seller
        sales_path
      when Buyer
        shops_path
      when Provider
        suppliers_path(resource.id)
      else
        super
      end
    else
      super
    end
  end

end
