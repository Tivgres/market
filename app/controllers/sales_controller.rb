# frozen_string_literal: true

class SalesController < ApplicationController
  before_action :authenticate_user!
  before_action :sale_params, except: %w[index new destroy]
  before_action :check_user
  before_action :prepare
  before_action :set_sale, only: %i[show edit update destroy]

  # GET /sales
  # GET /sales.json
  def index
    @sales = Sale.where(shop_id: current_user.shop_id).order('created_at DESC').page(params[:page]).per(25)
    @shop = @sales.first.shop
  end

  # GET /sales/new
  def new
    @sale = Sale.new
  end

  # POST /sales
  # POST /sales.json
  def create
    if current_user.is_a? Seller
      @sale = Sale.new(sale_params)
      @sale.shop_id = current_user.shop_id
      stock = Stock.where(shop_id: current_user.shop_id, product_id: sale_params[:product_id]).first
      @sale.price = stock.price * 1.25
      respond_to do |format|
        if @sale.save
          if stock.count == 1
            stock.destroy!
          else
            stock.count = stock.count - 1
            stock.save!
          end
          format.html { redirect_to sales_path, success: 'Sale was successfully created.' }
          format.json { render :show, status: :created, location: @sale }
        else
          format.html { render :new }
          format.json { render json: @sale.errors, status: :unprocessable_entity }
        end
      end
    else
      @shop = Shop.find(sale_params[:shop_id])
      @sale = Sale.new(sale_params)
      stock = Stock.where(shop_id: sale_params[:shop_id], product_id: sale_params[:product_id]).first
      @sale.price = stock.price * 1.25
      @sale.seller_id = Seller.where(shop_id: sale_params[:shop_id]).first.id
      respond_to do |format|
        if @sale.save!
          if stock.count == 1
            stock.destroy!
          else
            stock.count = stock.count - 1
            stock.save!
          end
          format.js {}
        else
          format.js {}
        end
      end
    end
  end


  # DELETE /sales/1
  # DELETE /sales/1.json
  def destroy
    sale = Sale.find(params[:id])
    if sale.destroy!
      redirect_to sales_path, success: 'Sale was successfully destroyed.'
    else
      render :index, danger: 'Sale was not destroyed.'
    end
  end

  private

  def prepare
    if current_user.is_a? Seller
      @products = Stock.where(shop_id: current_user.shop_id).map(&:product)
      shop = Shop.find(current_user.shop_id)
      if !shop.style.eql?('booths') && !shop.style.eql?('shopfront')
        @shop = shop
        @buyers = Buyer.all
      end
    end
  end

  def check_user
    #redirect_to root_path unless current_user.is_a? Seller
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_sale
    @sale = Sale.find(params[:id])
  end



  # Never trust parameters from the scary internet, only allow the white list through.
  def sale_params
    if current_user.is_a? Seller
      shop = Shop.find(current_user.shop_id)
      if shop.style.eql?('booths') || shop.style.eql?('shopfront')
        params.require(:sale).permit(:product_id, :seller_id)
      else
        params.require(:sale).permit(:product_id, :seller_id, :buyer_id)
      end
    else
      params.require(:sale).permit(:product_id, :shop_id, :buyer_id)
    end
  end
end
