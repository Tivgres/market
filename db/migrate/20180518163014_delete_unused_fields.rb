class DeleteUnusedFields < ActiveRecord::Migration[5.2]
  def change
    remove_column :sales, :supplier_id
  end
end
