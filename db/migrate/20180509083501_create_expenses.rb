class CreateExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :expenses do |t|
      t.integer :shop_id
      t.integer :price
      t.string :title
      t.timestamps
    end
  end
end
