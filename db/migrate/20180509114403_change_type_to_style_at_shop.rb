class ChangeTypeToStyleAtShop < ActiveRecord::Migration[5.2]
  def change
    remove_column :shops, :type
    add_column :shops, :style, :string
  end
end
