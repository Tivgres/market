class DropSellerProviderBuyer < ActiveRecord::Migration[5.2]
  def change
    drop_table :sellers
    drop_table :providers
    drop_table :buyers
  end
end
