class CreateSuppliers < ActiveRecord::Migration[5.2]
  def change
    create_table :suppliers do |t|
      t.integer :product_id
      t.integer :provider_id
      t.integer :price
      t.timestamps
    end
  end
end
