class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks do |t|
      t.integer :shop_id
      t.integer :product_id
      t.integer :count
      t.integer :price

      t.timestamps
    end
  end
end
