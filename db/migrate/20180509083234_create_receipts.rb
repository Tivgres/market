class CreateReceipts < ActiveRecord::Migration[5.2]
  def change
    create_table :receipts do |t|
      t.integer :product_id
      t.integer :shop_id
      t.integer :seller_id
      t.integer :buyer_id
      t.integer :price
      t.timestamps
    end
  end
end
