class AddSaleIdToReciept < ActiveRecord::Migration[5.2]
  def change
    add_column :receipts, :sale_id, :integer
  end
end
