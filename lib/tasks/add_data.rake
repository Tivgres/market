# frozen_string_literal: true

namespace :add_data do
  task preview_data: :environment do

    # DEF DATA
    post_email = '@example.com'
    def_password = '44332211'
    rand = Random.new

    # add shops
    5.times do
      Shop.create!(style: 'store')
      Shop.create!(style: 'shop')
      Shop.create!(style: 'booths')
      Shop.create!(style: 'shopfront')
    end

    # add sellers
    pre_email_seller = 'seller'
    sellers = %w[Yan Danna Maryanna Gerda Megan Hobert Selene Reginald Bettie Jules Keli Sena Lucilla Shizue Jesus Gwendolyn Emory Asha Sha Randall]
    shops = Shop.all
    sellers.each_with_index do |seller, index|
      Seller.create!(email: "#{pre_email_seller}#{index}#{post_email}",
                     password: def_password,
                     password_confirmation: def_password,
                     name: seller, shop_id: shops[index].id)
    end
    providers = %w[Caitlyn Essie George Rosanne Lourie Alexa Beatris Jody Lucretia Edgar]
    pre_email_providers = 'provider'

    # add providers
    providers.each_with_index do |provider, index|
      Provider.create!(email: "#{pre_email_providers}#{index}#{post_email}",
                       password: def_password,
                       password_confirmation: def_password,
                       name: provider)
    end

    # add buyers
    buyers = %w[Khadijah Jimmie Tracie Ellen Shani Georgene Nenita Kendra Cierra Carmelina Maybelle Shante Annelle Doreatha Tad Sylvie Marguerite Deloise Lisette Mac Karrie Robin Ernesto Bobette Tasia Tessie Janita Waltraud Daryl Valencia]
    pre_email_buyers = 'buyer'
    buyers.each_with_index do |buyer, index|
      Buyer.create!(email: "#{pre_email_buyers}#{index}#{post_email}",
                    password: def_password, password_confirmation: def_password,
                    name: buyer)
    end

    # add products
    vendors = %w[apple hp asus acer samsung toshiba xiaomi dell blackberry rpi opi wd]
    range_letters = ('A'..'Z').to_a
    501.times do
      Product.create!(title: "#{vendors[rand(vendors.size)]} #{range_letters[rand(range_letters.size)]}#{range_letters[rand(range_letters.size)]}#{rand(1000)}")
    end

    # add expenses
    Shop.all.each do |shop|
      Expense.create!(shop_id: shop.id, price: rand(5_000), title: 'Rent')
      Expense.create!(shop_id: shop.id, price: rand(10_000), title: 'Salaries')
    end

    # add suppliers
    providers = Provider.all
    products = Product.all
    products.each do |product|
      providers.each do |provider|
        Supplier.create!(product_id: product.id, provider_id: provider.id,
                         price: rand(20_000))
      end
    end

    # add sales
    shops = Shop.all
    products = Product.all
    sellers = Seller.all
    buyers = Buyer.all
    100.times do
      shop = shops[rand(shops.size)]
      product = products[rand(products.size)]
      supplier = Supplier.where(product_id: product.id).order('price').first
      if shop.style.equal?('booths') && shop.style.equal?('shopfront')
        Sale.create!(product_id: product.id, shop_id: shop.id,
                     seller_id: sellers[rand(sellers.size)].id,
                     price: (supplier.price * 1.25))
      else
        price = supplier.price * 1.35
        buyer = buyers[rand(buyers.size)]
        receipt = Receipt.create!(product_id: product.id, shop_id: shop.id,
                                  seller_id: sellers[rand(sellers.size)].id,
                                  buyer_id: buyer.id, price: price)
        Sale.create!(product_id: product.id, shop_id: shop.id,
                     seller_id: sellers[rand(sellers.size)].id,
                     price: price, buyer_id: buyer.id, receipt_id: receipt.id)
      end
    end

  end

  task add_stocks: :environment do
    # add stocks
    50.times do
      Shop.all.each do |shop|
        product = Product.find(rand(1..100))
        supplier = Supplier.where(product_id: product.id).order('price').first
        Stock.create!(shop_id: shop.id, product_id: product.id, count: rand(1..5), price: supplier.price * 1.25)
      end
    end
  end
end